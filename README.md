
![logo](docs/img/logo_squared_small.png)

# Remediate the Flag

Repository for the RTF Docker Templates.

Look at the main [README](https://github.com/sk4ddy/remediatetheflag/README.md) for more information.