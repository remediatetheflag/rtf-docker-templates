FROM ubuntu
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
	apt-get -y install autoconf \
	libtool pkg-config gcc g++ make libssl-dev \
	libpam0g-dev libjpeg-dev libx11-dev libxfixes-dev \
	libxrandr-dev  flex bison libxml2-dev intltool \
	xsltproc xutils-dev python-libxml2 g++ xutils libfuse-dev \
	libmp3lame-dev nasm libpixman-1-dev xserver-xorg-dev firefox \
	&& mkdir -p /build-box/

RUN cd /build-box \
	&& curl -s -L -O https://github.com/neutrinolabs/xrdp/releases/download/v0.9.3/xrdp-0.9.3.tar.gz \
	&& tar xf xrdp-0.9.3.tar.gz && cd xrdp-0.9.3/ \
	&& ./bootstrap && ./configure --enable-fuse --enable-mp3lame --enable-pixman && make && make install \
	&& ln -s /usr/local/sbin/xrdp /usr/sbin/ \
	&& ln -s /usr/local/sbin/xrdp-sesman /usr/sbin/ \
	&& xrdp-keygen xrdp auto 2048
	
RUN cd /build-box \
	&& curl -s -L -O https://github.com/neutrinolabs/xorgxrdp/releases/download/v0.2.3/xorgxrdp-0.2.3.tar.gz \
	&& tar xf xorgxrdp-0.2.3.tar.gz && cd xorgxrdp-0.2.3/ \
	&& ./bootstrap && ./configure && make && make install

RUN	apt-get clean && apt-get -y remove && apt autoremove -y && rm -rf /build-box && useradd -ms /bin/bash rtf

RUN	apt-get update && apt-get -y install xfce4 xubuntu-default-settings tomcat8 openjdk-8-jdk iptables unzip

COPY	base.zip /tmp/base.zip
RUN     unzip /tmp/base.zip -d /tmp/ \
		&& cp -Rf /tmp/fs/rtf/. /home/rtf \ 
		&& rm -rf /var/lib/tomcat8/webapps \ 
		&& mv /tmp/fs/webapps /var/lib/tomcat8/ \ 
		&& mv /tmp/fs/logo.png /home/logo.png \ 
		&& mv /tmp/fs/xrdp.ini /etc/xrdp/xrdp.ini \ 
		&& mv /tmp/run/init.sh /tmp/init.sh \ 
		&& rm -rf /tmp/fs && rm -rf /tmp/run && rm /tmp/base.zip

RUN 	touch /var/lib/tomcat8/rtf-exercise-agent.log && chown tomcat8:tomcat8 /var/lib/tomcat8/rtf-exercise-agent.log
RUN 	["chown", "-R", "tomcat8:tomcat8", "/var/lib/tomcat8/webapps"]
RUN 	["chmod", "-R", "777", "/home/rtf/"]
RUN 	["chmod", "+x", "/tmp/init.sh"]

EXPOSE 3389
EXPOSE 8080
ENTRYPOINT /tmp/init.sh